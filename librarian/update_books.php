<?php
 session_start();
  
 if (!isset($_SESSION["librarian"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 
 include"header.php";
 ?>

<?php require "connection.php";?>
<?php
if(isset($_GET['id'])){
    $id=$_GET['id'];
    $query="SELECT * FROM add_books WHERE id=$id";
    $fire=mysqli_query($link,$query) or die("cannot fetch the data".mysqli_error($link));
    

    $user=mysqli_fetch_assoc($fire);
    

}
?>
<!-- update data -->
<?php
 
        if(isset($_POST["update"])){
            $books_name=$_POST['booksname'];
            // $tn=md5(time());
            // $fnm=$_FILES["f1"]["name"];
            // $dst="./books_images/".$tn.$fnm;
            // $dst1="books_images/".$tn.$fnm;
            // move_uploaded_file($_FILES["f1"]["tmp_name"], $dst);
            $books_auther_name=$_POST['authername'];
            $books_description=$_POST['description'];
            $books_pubication_name=$_POST['pname'];
            $books_purchase_date=$_POST['bpurchasedt'];
            $books_price=$_POST['bprice'];
            $books_qty=$_POST['bqty'];
            $books_available_qty=$_POST['aqty'];
            $query="UPDATE add_books SET books_name='$books_name',books_auther_name='$books_auther_name',books_description='$books_description',books_pubication_name='$books_pubication_name',books_purchase_date='$books_purchase_date',books_price='$books_price',books_qty='$books_qty',books_available_qty='$books_available_qty' WHERE id=$id";
            $fire=mysqli_query($link,$query) or die("cannot update the data".mysqli_error($link));
            // if ($fire) echo "data executed successfully";
             ?>
        <script type="text/javascript">
            alert("Books Updated Successfully");
        </script>

        <?php

            }
            // mysqli_query($link,"UPDATE add_books SET VALUES('','$_POST[booksname]','$dst1','$_POST[authername]','$_POST[description]','$_POST[pname]','$_POST[bpurchasedt]','$_POST[bprice]','$_POST[bqty]','$_POST[aqty]','$_SESSION[librarian]')")

       
?>
 <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Library Management System</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Add Books</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <form name="form1" action="<?php $_SERVER['PHP_SELF']?>" method="post" class="col-lg-12" enctype="multipart/form-data">
                                <table class="table table-bordered">
                                    <tr>
                                        <td><input value="<?php echo $user['books_name'];?>" type="text" class="form-control" placeholder="Books_Name" name="booksname" required=""></td>
                                    </tr>
                                     <!-- <tr>
                                        <td>Books Image
                                            <input type="file" name="f1" required=""></td>
                                    </tr> -->
                                     <tr>
                                        <td><input value="<?php echo $user['books_auther_name'];?>" type="text" class="form-control" placeholder="Books_Auther_Name" name="authername" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input value="<?php echo $user['books_description'];?>" type="text" class="form-control" placeholder="Books_Description" name="description" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input value="<?php echo $user['books_pubication_name'];?>" type="text" class="form-control" placeholder="Books_Publication_Name" name="pname" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input value="<?php echo $user['books_purchase_date'];?>" type="text" class="form-control" placeholder="Books_Purchase_date" name="bpurchasedt" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input value="<?php echo $user['books_price'];?>" type="text" class="form-control" placeholder="Books_Price" name="bprice" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input value="<?php echo $user['books_qty'];?>" type="text" class="form-control" placeholder="Books_Quantity" name="bqty" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input value="<?php echo $user['books_available_qty'];?>" type="text" class="form-control" placeholder="Available Quantity" name="aqty" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input   type="submit" name="update" class= "btn btn-default submit" submit" value="Update Books Details" style="background-color: blue; color:white;margin:auto;
  display:block;padding:16px 32px;
  font-family:helvetica;
  font-size:16px;
  font-weight:100;
  color:#fff;
  background: #587286;
  border:0;
  font-weight:100;"></td>
                                    </tr>
                                </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php
include"footer.php";
?>