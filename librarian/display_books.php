<?php
 session_start();
 if (!isset($_SESSION["librarian"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 include"connection.php";
 include"header.php";
 ?>
 <!-- page content area main -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Library Management System</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Books Inaformation</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <form name="form1" action="" method="post">
                                    <input type="text" name="t1" class="form-control" placeholder="Enter Books Name">
                                    <input type="submit" name="submit1" value="Search books" class="btn btn-default">
                                </form>
                                <?php
                                if (isset($_POST["submit1"])) {
                                     $res=mysqli_query($link,"SELECT * FROM add_books WHERE books_name LIKE ('$_POST[t1]%')");
                                echo "<table class='table table-boedered'>";
                                echo "<tr>";
                                echo "<th>"; echo "Books Names";  echo "</th>";
                                echo "<th>"; echo "Image";  echo "</th>";
                                echo "<th>"; echo "Auther Name";  echo "</th>";
                                // echo "<th>"; echo "Publication Name";  echo "</th>";
                                // echo "<th>"; echo "Purchase Date";  echo "</th>";
                                // echo "<th>"; echo "Price";  echo "</th>";
                                // echo "<th>"; echo "Quantity";  echo "</th>";
                                // echo "<th>"; echo "Available Quantity";  echo "</th>";
                                echo "<th>"; echo "View info";  echo "</th>";
                                echo "<th>"; echo "Edit Books";  echo "</th>";
                                echo "<th>"; echo "Delete Books";  echo "</th>";
                                echo "</tr>";
                                while ($row=mysqli_fetch_array($res)) {
                                echo "<tr>";
                                echo "<td>"; echo $row["books_name"];  echo "</td>";
                                echo "<td>"; ?> <img src="<?php echo $row["books_image"]; ?>" height="100" width="100"><?php echo "</td>";
                                echo "<td>"; echo $row["books_auther_name"];  echo "</td>";
                                // echo "<td>"; echo $row["books_pubication_name"];  echo "</td>";
                                // echo "<td>"; echo $row["books_purchase_date"];  echo "</td>";
                                // echo "<td>"; echo $row["books_price"]; echo "</td>";
                                // echo "<td>"; echo $row["books_qty"];  echo "</td>";
                                // echo "<td>"; echo $row["books_available_qty"];  echo "</td>";
                                 $id=$row['id'];
                                echo "<td>";echo "<button data-id='".$id."' class='userinfo'>More Details</button>" ;echo "</td>";
                                  echo "<td>"; ?><a class='update' href="update_books.php?id=<?php echo $row["id"]; ?>"> Update</a>  <?php  echo "</td>";
                                 echo "<td>"; ?><a class='delete' href="delete_books.php?id=<?php echo $row["id"]; ?>"> Delete</a>  <?php  echo "</td>";
                                echo "</tr>";
                                  
                                }

                                echo"</table>";
                                }
                                else{


                                $res=mysqli_query($link,"SELECT * FROM add_books");
                                echo "<table class='table table-boedered'>";
                                echo "<tr>";
                                echo "<th>"; echo "Books Names";  echo "</th>";
                                echo "<th>"; echo "Image";  echo "</th>";
                                echo "<th>"; echo "Auther Name";  echo "</th>";
                                // echo "<th>"; echo "Publication Name";  echo "</th>";
                                // echo "<th>"; echo "Purchase Date";  echo "</th>";
                                // echo "<th>"; echo "Price";  echo "</th>";
                                // echo "<th>"; echo "Quantity";  echo "</th>";
                                // echo "<th>"; echo "Available Quantity";  echo "</th>";
                                echo "<th>"; echo "View info";  echo "</th>";
                                 echo "<th>"; echo "Edit Books";  echo "</th>";
                                echo "<th>"; echo "Delete Books";  echo "</th>";
                                echo "</tr>";
                                while ($row=mysqli_fetch_array($res)) {
                                echo "<tr>";
                                echo "<td>"; echo $row["books_name"];  echo "</td>";
                                echo "<td>"; ?> <img src="<?php echo $row["books_image"]; ?>" height="100" width="100"><?php echo "</td>";
                                echo "<td>"; echo $row["books_auther_name"];  echo "</td>";
                                // echo "<td>"; echo $row["books_pubication_name"];  echo "</td>";
                                // echo "<td>"; echo $row["books_purchase_date"];  echo "</td>";
                                // echo "<td>"; echo $row["books_price"]; echo "</td>";
                                // echo "<td>"; echo $row["books_qty"];  echo "</td>";
                                // echo "<td>"; echo $row["books_available_qty"];  echo "</td>";
                                $id=$row['id'];
                                echo "<td>";echo "<button data-id='".$id."' class='userinfo'>More Details</button>" ;echo "</td>";
                                 echo "<td>"; ?><a class='update' href="update_books.php?id=<?php echo $row["id"]; ?>"> Update</a>  <?php  echo "</td>";
                                 echo "<td>"; ?><a class='delete' href="delete_books.php?id=<?php echo $row["id"]; ?>"> Delete</a>  <?php  echo "</td>";
                                echo "</tr>";
                                  
                                }

                                echo"</table>";
                            }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
         <div class="container" >
   <!-- Modal -->
   <div class="modal fade" id="empModal" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Books Additional Information</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
 
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
     </div>
    </div>
   </div>

<script type="text/javascript">
   $(document).ready(function(){

 $('.userinfo').click(function(){
   
   var userid = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'ajaxfile_books_details.php',
    type: 'post',
    data: {userid: userid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#empModal').modal('show'); 
    }
  });
 });
});
    
</script>
<?php
include"footer.php";
?>