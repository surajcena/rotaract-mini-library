<?php
 session_start();
  
 if (!isset($_SESSION["librarian"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 include"header.php";
 include"connection.php";
 ?>
 <!-- page content area main -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Library Management System</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Issue Books</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <form name="form1" action="" method="post">
                                    <table>
                                        <tr>
                                               <td>
                                                <select name="enr"  class="form-control selectpicker">
                                                    <?php
                                                    $res=mysqli_query($link,"SELECT enrollment FROM student_registration");
                                                    
                                                    while ($row=mysqli_fetch_array($res)) {
                                                        echo "<option>";
                                                        echo $row["enrollment"];
                                                        echo "</option>";
                                                    }
                                                    ?>
                                                </select>
                                                </td>
                                                    
                                                <td>
                                                    <input type="submit" value="search" name="submit1" class="form-control btn btn-default" style="margin-top: 5px">
                                                 </td>
                                        </tr>
                                    </table>
                                
                                <?php
                                if (isset($_POST["submit1"])) {
                                     $main= $_POST['enr'];
                                    $res=mysqli_query($link,"SELECT * FROM student_registration WHERE enrollment='".$main."'" );
                                    while ($row=mysqli_fetch_array($res)) {
                                      $firstname=$row["firstname"];
                                      // echo "$firstname";
                                      $lastname=$row["lastname"];
                                      $username=$row["username"];
                                      $email=$row["email"];
                                      $contact=$row["contact"];
                                      $sem=$row["sem"];
                                      $enrollment=$row["enrollment"];
                                      $_SESSION["enrollment"]=$enrollment;
                                      $_SESSION["username"]=$username;


                                    //echo  $_POST['enr'];

                                    }
                                    ?>

                                     <table class="table table-bordered">
                                    <tr>
                                         
                                        <td><input type="text" class="form-control" placeholder="enrollmentno" name="enrollment" value="<?php  echo $_POST['enr']; ?>" disabled></td>
                                    </tr>
                                      <tr>
                                        <td><input type="text" class="form-control" placeholder="studentname" name="studentname" value="<?php echo $firstname.' '.$lastname;?>" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input type="text" class="form-control" placeholder="studentsem" name="studentsem" value="<?php echo $sem ;?>" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input type="text" class="form-control" placeholder="studentcontact" name="studentcontact" value="<?php echo $contact ; ?>" required=""></td>
                                    </tr>
                                      <tr>
                                        <td><input type="text" class="form-control" placeholder="studentemail" name="studentemail" value="<?php echo $email; ?>" required=""></td>
                                    </tr>
                                      <tr>
                                        <td>
                                            <select name="booksname" class="form-control selectpicker">
                                                <?php

                                                $res=mysqli_query($link,"SELECT books_name FROM add_books");
                                                while ($row=mysqli_fetch_array($res)) {
                                                    echo "<option>";
                                                    
                                                    echo $row["books_name"];

                                                    echo "</option>";
                                                }
                                                ?>
                                               
                                            </select>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td><input type="text" class="form-control" placeholder="bookissuedate" name="bookissuedate" value="<?php echo date("d-M-Y"); ?>" required=""></td>
                                    </tr>
                                     <tr>
                                        <td><input type="text" class="form-control" placeholder="studentusername" name="studentusername" value="<?php echo $username ?>" disabled="" ></td>
                                    </tr>
                                     <tr>
                                        <td><input type="submit" value="issue books"  name="submit2" class="form-control btn btn-default" style="background-color: blue;color: white;"></td>
                                    </tr>
                                </table>
                                    <?php
                                    
                                }

                                ?>
                            </form>
                            <?php
                            $qty=0;
                            if (isset($_POST["submit2"])) {
                               $res=mysqli_query($link,"SELECT * FROM add_books WHERE books_name='$_POST[booksname]'");
                               while ($row=mysqli_fetch_array($res)) {
                                   $qty=$row["books_available_qty"];
                               }
                               if ($qty==0) {
                                 ?>
                                   <div class="alert alert-danger col-lg-6 col-lg-push-3">
                               <strong style="color:white">This books are not available in stock.</strong> 
                                 </div>
                                 <?php
                                   
                               }
                               else{

                                mysqli_query($link,"INSERT INTO issue_books VALUES('','$_SESSION[enrollment]','$_POST[studentname]','$_POST[studentsem]','$_POST[studentcontact]','$_POST[studentemail]','$_POST[booksname]','$_POST[bookissuedate]','',' $_SESSION[username]')");
                                mysqli_query($link,"UPDATE add_books SET books_available_qty=books_available_qty-1 WHERE books_name='$_POST[booksname]'");
                                ?>
                                <script type="text/javascript">
                                    alert("Books issue successfully");
                                    window.location.herf=window.location.herf;
                                </script>
                                <?php
                            }
                            }

                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
<?php
include"footer.php";
?>