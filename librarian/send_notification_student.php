<?php
 session_start();
  
 if (!isset($_SESSION["librarian"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 include"header.php";
 include"connection.php";
 ?>
 <!-- page content area main -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Library Management System</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Send Message To Student</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                 <form name="form1" action="" method="post" class="col-lg-12" enctype="multipart/form-data">
                                <table class="table table-bordered">
                                    <tr>
                                        
                                        <select class="form-control" name="dusername">
                                            <?php
                                             $res=mysqli_query($link,"SELECT * FROM student_registration");
                                             while ($row=mysqli_fetch_array($res)) {
                                                 ?><option value="<?php echo $row["username"]?>">
                                                     <?php echo $row["username"]."(".$row["enrollment"] .")"; ?>
                                                 </option>  <?php
                                             }

                                            ?>
                                            
                                        </select>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" name="title" placeholder="Enter Title" required="">
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            Message
                                            <textarea name="msg" class="form-control" required=""></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" placeholder="message_date" name="message_date" value="<?php echo date("d-M-Y"); ?>" readonly ></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="submit" name="submit1" value="Send Message"  style="  
color: #fff !important;
text-transform: uppercase;
text-decoration: none;
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(39,39,106,1) 35%, rgba(0,212,255,1) 100%);
padding: 3px;
border-radius: 10px;
display: inline-block;
border: none;
transition: all 0.4s ease 0s;">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <?php
        if (isset($_POST["submit1"])) {
            $res=mysqli_query($link,"INSERT INTO messages VALUES('','$_SESSION[librarian]','$_POST[dusername]','$_POST[title]','$_POST[msg]','$_POST[message_date]','n')");
        ?>
        <script type="text/javascript">
            alert("Message send successfully");
        </script>
        <?php


        }

        ?>
<?php
include"footer.php";
?>