<?php
  session_start();
 if (!isset($_SESSION["librarian"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 include"header.php";
 include"connection.php";
  
//  Header ("Content-type: text/css; css/copyright.css ");?>

 <!-- page content area main -->
 <!--<link rel="stylesheet" type="text/css" href="css/copyright.css">-->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Library Management System</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:600px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>User Information</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <?php
                                $res=mysqli_query($link,"SELECT * FROM student_registration");
                                echo "<table class='table table_bordered'>";
                                echo"<tr>";
                                echo"<th>";echo "Firstname";echo "</th>";
                                echo"<th>";echo "Lastname";echo "</th>";
                                // echo"<th>";echo "Username";echo "</th>";
                                // echo"<th>";echo "Email";echo "</th>";
                                // echo"<th>";echo "Contact";echo "</th>";
                                // echo"<th>";echo "Permanent Address";echo "</th>";
                                echo"<th>";echo "Enrollment";echo "</th>";
                                echo"<th>";echo "Id Image";echo "</th>";
                                echo"<th>";echo "Status";echo "</th>";
                                echo"<th>";echo "View";echo "</th>";
                                echo"<th class='approve1'>";echo "Approve";echo "</th>";
                                echo"<th class='notapprove1'>";echo "NotApprove";echo "</th>";
                                echo "</tr>";
                                while ($row=mysqli_fetch_array($res)) {
                                    echo"<tr>";
                                echo"<td>";echo $row["firstname"];echo "</td>";
                                echo"<td>";echo $row["lastname"];echo "</td>";
                                // echo"<td>";echo $row["username"];echo "</td>";
                                // echo"<td>";echo $row["email"];echo "</td>";
                                // echo"<td>";echo $row["contact"];echo "</td>";
                                // echo"<td>";echo $row["sem"];echo "</td>";
                                echo"<td>";echo $row["enrollment"];echo "</td>";
                                echo "<td>"; ?> <img src="../student/<?php echo $row["id_images"] ?>" height="100" width="100"><?php echo "</td>";
                                echo"<td>";echo $row["status"];echo "</td>";
                                $id=$row['id'];
                                echo "<td>";echo "<button data-id='".$id."' class='userinfo'>More Details</button>" ;echo "</td>";
                                echo "<td>"; ?> <a class='approve' href="approve.php?id= <?php echo $row["id"];?> ">Approve</a><?php  echo "</td>";
                                echo "<td>"; ?> <a class='notapprove' href="notapprove.php?id= <?php echo $row["id"];?> ">NotApprove</a><?php  echo "</td>";
                                 echo "</tr>";
                                 // echo "</table>"; 
                                }
                                echo "</table>"; 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
 <div class="container" >
   <!-- Modal -->
   <div class="modal fade" id="empModal" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Readers Additional Information</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
 
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
     </div>
    </div>
   </div>

<script type="text/javascript">
   $(document).ready(function(){

 $('.userinfo').click(function(){
   
   var userid = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'ajaxfilelib.php',
    type: 'post',
    data: {userid: userid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#empModal').modal('show'); 
    }
  });
 });
});
    
</script>

<?php
include"footer.php";
?>