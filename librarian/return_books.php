<?php
  session_start();
 if (!isset($_SESSION["librarian"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 include"header.php";
 include"connection.php";
 ?>
 <!-- page content area main -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Library Management System</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Return Books</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                              <form name="form1" action="" method="post">
                                  <table class="table table-bordered">
                                      <tr>
                                          <td><select name="enr" class="form-control">
                                             <?php
                                             $res=mysqli_query($link,"SELECT student_enrollment FROM issue_books WHERE books_return_date=''");
                                             while ($row=mysqli_fetch_array($res)) {
                                                 echo "<option>";
                                                 echo $row["student_enrollment"];
                                                 echo "</option>";
                                             }
                                             ?>
                                          </select>
                                              
                                          </td>
                                          <td>
                                              <input type="submit" name="submit1" value="Search" class="form-control" style="background-color: blue;color: white">
                                          </td>
                                      </tr>
                                  </table>
                              </form>
                              <?php
                              if (isset($_POST["submit1"])) {
                                  $res=mysqli_query($link,"SELECT * FROM issue_books WHERE student_enrollment='$_POST[enr]'");
                                             
                              
                              echo "<table class='table table-bordered'>";
                              echo"<tr>";
                              // echo "<th>";
                              // echo "student_enrollment";
                              // echo "</th>";
                              //   echo "<th>";
                              // echo "Student Name";
                              // echo "</th>";
                              //   echo "<th>";
                              // echo "Student_Sem";
                              // echo "</th>";
                              //   echo "<th>";
                              // echo "Student Contact";
                              // echo "</th>";
                              //   echo "<th>";
                              // echo "Student Email";
                              // echo "</th>";
                              echo "<th>";
                              echo "Books Name";
                              echo "</th>";
                              echo "<th>";
                              echo "Books Issue Date";
                              echo "</th>";
                              echo "<th>";
                              echo "Reader info"; 
                              echo "</th>";
                              echo "<th>";
                              echo "Return Books";
                              echo "</th>";
                               echo "<th>";
                              echo "Books Returned Date";
                              echo "</th>";

                              echo "</tr>";
                            while ($row=mysqli_fetch_array($res)) {
                                echo "<tr>";
                                // echo "<td>"; echo $row["student_enrollment"]; echo "</td>";
                                // echo "<td>"; echo $row["student_name"]; echo "</td>";
                                // echo "<td>"; echo $row["student_sem"]; echo "</td>";
                                // echo "<td>"; echo $row["student_contact"]; echo "</td>";
                                // echo "<td>"; echo $row["student_email"]; echo "</td>";
                                echo "<td>"; echo $row["books_name"]; echo "</td>";
                                echo "<td>"; echo $row["books_issue_date"]; echo "</td>";
                                 $id=$row['id'];
                                echo "<td>";echo "<button data-id='".$id."' class='userinfo'>More Details</button>" ;echo "</td>";
                                 echo "<td>"; ?> <a class='return' href="return.php?id=<?php echo $row["id"] ?>">Returned</a> <?php echo "</td>";
                                 echo "<td>"; echo $row["books_return_date"]; echo "</td>";
                                echo "</tr>";


                            }
                              echo "</table>";
                          }
                              ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

          <div class="container" >
   <!-- Modal -->
   <div class="modal fade" id="empModal" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"> Reader Additional Information</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
 
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
     </div>
    </div>
   </div>

<script type="text/javascript">
   $(document).ready(function(){

 $('.userinfo').click(function(){
   
   var userid = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'ajaxfile_issued_books.php',
    type: 'post',
    data: {userid: userid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#empModal').modal('show'); 
    }
  });
 });
});
    
</script>
<?php
include"footer.php";
?>