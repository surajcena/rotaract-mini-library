<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    
    <!-- Title Tag -->
    <title>Rotaract library</title>
<!--

November Template

http://www.templatemo.com/tm-473-november

-->
    <!-- <<Mobile Viewport Code>> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            
    <!-- <<Attched Stylesheets>> -->
    <link rel="stylesheet" href="css/theme.css" type="text/css" />
    <link rel="stylesheet" href="css/media.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,600italic,400italic,800,700' rel='stylesheet' type='text/css'>    
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>

</head>
<body>

<!-- \\ Begin Holder \\ -->
<div class="DesignHolder">
	<!-- \\ Begin Frame \\ -->
	<div class="LayoutFrame">
        <!-- \\ Begin Header \\ -->
        <header>
            <div class="Center">
                <div class="site-logo">
                	<h1><a href="#">Mi<span>Ni</span>Library</a></h1>
                </div>
               <div id="mobile_sec">
               <div class="mobile"><i class="fa fa-bars"></i><i class="fa fa-times"></i></div>
                <div class="menumobile">
                    <!-- \\ Begin Navigation \\ -->
                    <nav class="Navigation">
                        <ul>
                            <li class="active">                                
                                <a href="#home">Home</a>
                                <span class="menu-item-bg"></span>
                            </li>
                            <li>
                                <a href="student/login.php">Login</a>
                                <span class="menu-item-bg"></span>
                            </li>
                            <li>
                                <a href="student/registration.php">Registration</a>
                                <span class="menu-item-bg"></span>
                            </li>
                            <!-- <li>
                                <a href="librarian/login.php">Admin Login</a>
                                <span class="menu-item-bg"></span>
                            </li> -->
                            <li>
                                <a href="#contact">Contact</a>
                                <span class="menu-item-bg"></span>
                            </li>
                        </ul>
                    </nav>
                    <!-- // End Navigation // -->
				</div>
				</div>
                <div class="clear"></div>
            </div>
        </header>
        <!-- // End Header // -->
        <!-- \\ Begin Banner Section \\ -->
        <div class="Banner_sec" id="home">
            <!--  \\ Begin banner Side -->
            <div class="bannerside">
	            <div class="Center">
                    <!--  \\ Begin Left Side -->
                    <div class="leftside">
                        <h3>Rotaract<span>Mini Library</span></h3>
                        <p>  with the aim to develop the learning habit on youth and to make the books easily accessible from readers..</p>
                        <a href="#about">MORE DETAILS</a>
                    </div>                        								
                    <!--  // End Left Side // -->
                    <!--  \\ Begin Right Side -->
                    <div class="rightside">
                    	<ul id="slider">	
                    		<li>
                                <div class="Slider">
                                    <figure><img src="img/library20.jpg" alt="image"></figure>
                                    <!-- <div class="text">
                                        <div class="Icon">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-heart"></i>700</a></li>
                                                <li><a href="#"><i class="fa fa-commenting"></i>150</a></li>
                                            </ul>	
                                        </div>                        								
                                        <div class="Lorem">
                                            <p>lorem quis bibendum <span>Necagittis Nibel</span></p>
                                        </div>
                                    </div> -->
                                </div>
							</li>
                    		<li>
                                <div class="Slider">
                                    <figure><img src="img/library21.jpg" alt="image"></figure>
                                    <!-- <div class="text">
                                        <div class="Icon">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-heart"></i>700</a></li>
                                                <li class="num"><a href="#"><i class="fa fa-commenting"></i>150</a></li>
                                            </ul>	
                                        </div>                        								
                                        <div class="Lorem">
                                            <p>lorem quis bibendum<span>Necagittis Nibel</span></p>
                                        </div>
                                    </div> -->
                                </div>
							</li>
						</ul>                                                            
            	        <figure><img src="img/Shadow-img.png" alt="image" class="Shadow"></figure>                                                        
                    </div>
                    <!--  // End Right Side // -->
	            </div>
            </div>
            <!--  // End banner Side // -->
            <div class="clear"></div>
        </div>
        <!-- // End Banner Section // -->
         <div class="bgcolor"></div>
        <!-- \\ Begin Container \\ -->
        <div id="Container">
            <!-- \\ Begin About Section \\ -->
            <div class="About_sec" id="about">
                <div class="Center">            	
                    <h2>about us</h2>            		
                    <p>Rotaract Mini Library (RML) was founded on 2074 BS on Sarswati Puja, with the aim to develop the learning habit on youth and to make the books easily accessible from youths.After being member of  Mini Library, you can hire books in minimal amount for certain days. This is project of Rotract Club of Lakeside Pokhara to support "Literacy Mission".This Library is an independent body of club itself and will be executed by committee of club members. Rotaract Mini library is temporarily situated at Trip np office, Newroad, Pokhara.</p>
                    <div class="Line"></div>	
                    <!-- \\ Begin Tab side \\ -->
                    <div class="Tabside">
                        <ul>
                            <li><a href="javascript:;" class="tabLink activeLink" id="cont-1">Mision</a></li>
                            <li><a href="javascript:;" class="tabLink" id="cont-2">vision</a></li>
                            <!-- <li><a href="javascript:;" class="tabLink" id="cont-3">Sponsors</a></li> -->
                        </ul>
                      <div class="clear"></div>
                        <div class="tabcontent" id="cont-1-1">
                            <div class="TabImage">
                                <div class="img1">
                                    <figure><img src="img/books1.jpg" alt="image"></figure>	
                                </div>
                                <div class="img2">
                                    <figure><img src="img/library.jpg" alt="image"></figure>
                                </div>
                            </div>
                            <div class="Description">
                                <h3>Rotaract Club Of Pokhara Lakeside<span>Misson</span></h3>
                                <p>This is project of Rotract Club of Lakeside Pokhara to support "Literacy Mission"</p>
                                <p></p>
                            </div>
                        </div>
                        <div class="tabcontent hide" id="cont-2-1">
                            <div class="TabImage">
                                <div class="img1">
                                    <figure><img src="img/books1.jpg" alt="image"></figure>	
                                </div>
                                <div class="img2">
                                    <figure><img src="img/library.jpg" alt="image"></figure>
                                </div>
                            </div>
                            <div class="Description">
                                <h3>Rotaract Club Of Pokhara Lakeside<span>There are two clear vision of the Rotaract Mini Library.</span></h3>
                                <p> A) To make the different  books (Novel, Story, Poem, Biography and other) easily accessible from youths at minimal price. </p>
                                <p> B) To promote the reading habit among the youths.</p>
                            </div>
                        </div>
                        <div class="tabcontent hide" id="cont-3-1">
                            <div class="TabImage">
                                <div class="img1">
                                    <figure><img src="img/about-img2.jpg" alt="image"></figure>	
                                </div>
                                <div class="img2">
                                    <figure><img src="img/about-img1.jpg" alt="image"></figure>
                                </div>
                            </div>
                            <div class="Description">
                                <h3>Donec molestie malesuada nisl <span>Aenean eget consequat diam</span></h3>
                                <p>Nullam at sem non enim aliquam ultrices non quis magna. In interdum interdum magna vitae accumsan. Etiam turpis tortor, malesuada vitae metus non, pharetra auctor mi. Pellentesque tincidunt enim vitae tincidunt euismod. Integer id ex enim. Nullam euismod efficitur libero quis interdum.</p>
                                <p>Phasellus porttitor tempus nibh, id luctus nibh porta ac. Nunc sed metus est. Proin ut nisi metus. Duis consectetur purus iaculis ornare suscipit.</p>
                            </div>
                        </div>
	                    <div class="clear"></div>	
                    </div>                    
                    <!-- // End Tab Side // -->
                </div>
            </div>
            <!-- // End About Section // -->
        <!-- \\ Begin Services Section \\ -->
        <div class="Services_sec" id="services">
            <div class="Center">
                <h2>our Services</h2>
                <p> After being member of the Rotaract Mini Library, we can take one book at a time and have to return it within 10 days. Additional 5 days can be added by paying extra amount.You have to return the book in as it is contidion.</p>		
                <div class="Line"></div>
                <!-- \\ Begin Services Side  \\ -->
                <div class="Serviceside">
                    <ul>
	                    <li class="Development"><a href="#services"><h4>DEVELOPMENT</h4></a></li>
    	                <li class="Desdin"><a href="#services"><h4>DESIGN</h4></a></li>
	                    <li class="Concept"><a href="#services"><h4>CONCEPT</h4></a></li>
	                    <li class="System"><a href="#services"><h4>SYSTEM</h4></a></li>
                    </ul>
                </div>
                <!-- // End Services Side // -->
            </div>                
        </div>
        <!-- // End Services Section // -->
        <!-- \\ Begin Pricing Section \\ -->
        <div class="Pricing_sec" id="pricing" style="background-color: white;">
            <div class="Center">
                <h2>Pricing</h2>
                <!-- <p>All plans come with unlimited disk space. Our support can be as quick as 15 minutes to get a response. Sed non<br>
                mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu.</p> -->
                <div class="Line"></div>
                <!-- \\ Begin Pricing Side \\ -->
                <div class="Pricingside">
                     <div class="Description">
                                <!-- <h3>Rotaract Club Of Pokhara Lakeside<span>There are two clear vision of the Pokhara Mini Library.</span></h3> -->
                                
                                <p> The price for the intial 10 days after you took book from the Mini Library is NRS 20 for all books. If you want additional 5 days, again you have to pay another NRS 20. If the member didn't return till the end of initial 15 days, you have pay NRS 5 fine per day until you return.If the book is lost or you couldn't provide it, you have to pay its market price or brought a new book from the market and also pay a fine.</p>
                                <div class="Center">
                                <h1>How to be a member of the Rotaract Mini Library?</h1>
                                 </div>
                                <h3>To be a member of the Rotaract Mini Library you have to fulfill the following three conditions.</h3>
                                <p>A) You should provide a book (Novel/ Story/ Biography/Poem/ Essay) worth around NRS 400 in the market while buying new one.</p>
                                <p>B) If you don't have such books, you can be a member by paying NRS 300.</p>
                                <p>C) After you fulfill the above conditions, you have to register your member id in the site of Rotaract Mini Library, which will provide the information about the books available in Mini Library.</p>

                            </div>
                    <!-- <ul>
                        <li>
                            <div class="Basic">
	                            <h5>basic</h5>
                            </div>
                            <div class="Dollar">
    	                        <h2>$27.50</h2>
                            </div>
                            <div class="Band">
	                            <p>The price for the intial 10 days after you took book from the Mini Library is NRS 20 for all books. If you want additional 5 days, again you have to pay another NRS 20. If the member didn't return till the end of initial 15 days, you have pay NRS 5 fine per day until you return <span>Band width</span></p>
                            </div>
                            <div class="Band">
	                            <p>32 GB<span>memory</span></p>
                            </div>
                            <div class="Band">
	                            <p>Support<span>24 Hours</span></p>
                            </div>
                            <div class="Band last">
	                            <p>Update<span>$20</span></p>
                            </div>
                            <div class="Order">
	                            <a href="#">order now</a>
                            </div>
                        </li>
                        <li>
                            <div class="Basic">
	                            <h5>Biz</h5>
                            </div>
                            <div class="Dollar">
	                            <h2>$44.50</h2>
                            </div>
                            <div class="Band">
	                            <p>5,500 GB <span>Band width</span></p>
                            </div>
                            <div class="Band">
	                            <p>64 GB<span>memory</span></p>
                            </div>
                            <div class="Band">
	                            <p>Support<span>1 Hour</span></p>
                            </div>
                            <div class="Band last">
	                            <p>Update<span>$10</span></p>
                            </div>
                            <div class="Order">
	                            <a href="#">order now</a>
                            </div>
                        </li>
                        <li>
                            <div class="Basic">
	                            <h5>Pro</h5>
                            </div>
                            <div class="Dollar">
	                            <h2>$72.50</h2>
                            </div>
                            <div class="Band">
	                            <p>12,000 GB <span>Band width</span></p>
                            </div>
                            <div class="Band">
	                            <p>128 GB<span>memory</span></p>
                            </div>
                            <div class="Band">
	                            <p>Support<span>15 Mins</span></p>
                            </div>
                            <div class="Band last">
	                            <p>Update<span>Free</span></p>
                            </div>
                            <div class="Order">
	                            <a href="#">order now</a>
                            </div>
                        </li>
                    </ul>  -->               	
                </div>
                <!-- // End Pricing Side // -->
            </div>
        </div>
        <!-- // End Pricing Section // -->
        <!-- \\ Begin Contact Section \\ -->
        <div class="Contact_sec" id="contact">
            <div class="Contactside">
                <div class="Center">
                    <h2>contact us</h2>
                    <p>Opening Time: 11-5 (except Saturday and major festivals)
                </p>
                    <div class="Line"></div>
                </div>  

            </div>
                
                <!-- \\ Begin Get Section \\ -->
                <div class="Get_sec">
                    <div class="Mid">					
                        <!-- \\ Begin Left Side \\ -->
                        <div class="Leftside">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.741814311501!2d83.98372091454988!3d28.2151558096001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995959c484ef34f%3A0xd55fcd31ab2ea842!2sTripnp%20Technologies%20Pvt.%20Ltd.!5e0!3m2!1sen!2snp!4v1580535984031!5m2!1sen!2snp" width="500" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                        <!-- // End Left Side // -->
                        <!-- \\ Begin Right Side \\ -->
                        <div class="Rightside">
                            <h3>Get in touch !</h3>
                                <address>
                                    New Rd, Pokhara 33700<br>नयाँ सडक, पोखरा 33700
                                </address>	
                                <address class="Number">
                                    Coordinators</br>Yagya Raj Adhikari (+977) 9846729033<br>Sagar Thapa (+977) <br>Ganesh Gautam(+977) <br>Saroj Nepali(+977) 
                                </address>	
                                <address class="Email">
                                    <a href="mailto:info@minilibrary.com">info@minilibrary.com</a>
                                </address>	
                                <div class="clear"></div>
                                <ul>
                                    <li><a rel="nofollow" href="http://www.facebook.com/templatemo"
                                   target="_parent"><img src="img/facebook-icn.png" alt="image"></a></li>
                                    <li><a href="#"><img src="img/twitter-icn.png" alt="image"></a></li>
                                    <li><a href="#"><img src="img/google-plus-icn.png" alt="image"></a></li>
                                    <li><a href="librarian/login.php"><img src="img/security.png" alt="image"></a></li>
                                </ul>
                        </div>
                        <!-- // End Right Side // -->
                    </div>
                    <!-- \\ Begin Footer \\-->
                    <footer>
                        <div class="Cntr">                
                            <p>COPYRIGHT © 2020 Rotaract Club Of Pokhara Lakeside.<br> DEVELOPER: <a rel="nofollow" href="http://www.suraztimilsina.com.np" target="_parent">SURAJ TIMiLSINA</a></p>
                        </div>
                    </footer>
                    <!-- // End Footer // -->
                </div>
                <!-- // End Get Section // -->
            
            </div>
            <!-- // End Contact Section // -->
        </div>
        <!-- // End Container // -->
	</div>
	<!-- // End Layout Frame // -->
</div>
<!-- // End Design Holder // -->
<div id="loader-wrapper">
<div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>

<!-- <<Attched Javascripts>> -->
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/jquery.sudoSlider.min.js"></script>
<script type="text/javascript" src="js/global.js"></script>
<script type="text/javascript" src="js/modernizr.js"></script>

</body>
</html>