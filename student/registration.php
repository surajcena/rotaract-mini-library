<?php
include'connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>User Registration Form | LMS </title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/custom.min.css" rel="stylesheet">
   
    <style type="text/css">
        .login{
            /*background: lightblue  no-repeat fixed center;*/
            /*background-image: linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(171,58,58,1) 50%, rgba(252,176,69,1) 100%);*/
            /*background-image: url('background.png');*/

        }
    </style>
</head>

<br>

<div class="col-lg-12 text-center ">

    <h1 style="font-family:Lucida Console;">Library Management System</h1>
</div>


<body class="login" style="margin-top: -20px; color: #2bc68f; background-color: #ffffff;">

<?php
$query="select * from student_registration order by id desc limit 1 ";
$result=mysqli_query($link,$query);
$row=mysqli_fetch_array($result);
$lastid=$row['id'];
if ($lastid == " ") {
    $empid="READER NO 1";
}
else{
//     $empid=substr($lastid,3);
//     $empid=intval($empid);
    $empid="READER NO ".($lastid+1);
}


?>

    <div class="login_wrapper">

            <section class="login_content" style="margin-top: -40px;">
                
                <form class="main" name="form1" action="" method="post" enctype="multipart/form-data">
                      
                    <h2>User Registration Form</h2><br>

                    <div>
                        <input type="text" class="form-control" placeholder="FirstName" name="firstname" required=""/>
                    </div>
                    <div>
                        <input type="text" class="form-control" placeholder="LastName" name="lastname" required=""/>
                    </div>

                    <div>
                        <input type="text" class="form-control" placeholder="Username" name="username" required=""/>
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" name="password" required=""/>
                    </div>
                    <div>
                        <input type="text" class="form-control" placeholder="email" name="email" required=""/>
                    </div>
                    <div>
                        <input type="text" class="form-control" placeholder="contact" name="contact" required=""/>
                    </div>
                     <div>
                        <input type="text" class="form-control" placeholder="Additional contact(if any)" name="acontact"/>
                    </div>
                    <div>
                        <input type="text" class="form-control" placeholder="Permanent Address" name="sem" required=""/>
                    </div>
                    <div>
                        <input type="text" class="form-control" placeholder="Temporary Address(if any)" name="temporary" />
                    </div>
                    <div>
                        <input type="text" class="form-control" name="enrollmentno" value="<?php  echo $empid; ?>" readonly />
                    </div>
                    <div>
                        <p style="text-align: left;">Your id card ,Citizenship,License front image</p>
                         <input type="file" name="f1" required=""></td>
                    </div>
                    <br>
                    <div class="col-lg-12  col-lg-push-3">
                        <input class="btn btn-default submit " style="  
color: #fff !important;
text-transform: uppercase;
text-decoration: none;
background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(39,39,106,1) 35%, rgba(0,212,255,1) 100%);
padding: 3px;
border-radius: 10px;
display: inline-block;
border: none;
transition: all 0.4s ease 0s;" type="submit" name="submit1" value="Register">
                    </div>
                    
                <a class="btn btn-default" style="
color: #fff !important;
text-transform: uppercase;
text-decoration: none;
background:linear-gradient(to right top, #051937, #004d7a, #008793, #00bf72, #a8eb12);
padding: 3px;
border-radius: 10px;
display: inline-block;
border: none;
transition: all 0.4s ease 0s;
                " href="../index.php">
            Go back to home page
            </a>

                </form>
            </section>

<?php
if (isset($_POST["submit1"]))
{
            $tn=md5(time());
            $fnm=$_FILES["f1"]["name"];
            $dst="./id_images/".$tn.$fnm;
            $dst1="id_images/".$tn.$fnm;
            move_uploaded_file($_FILES["f1"]["tmp_name"], $dst);

mysqli_query($link,"INSERT INTO  student_registration VALUES('','$_POST[firstname]','$_POST[lastname]','$_POST[username]','$_POST[password]','$_POST[email]','$_POST[contact]','$_POST[acontact]','$_POST[sem]','$_POST[temporary]','$_POST[enrollmentno]','$dst1','no')");
 ?>
 <script type="text/javascript">
            alert("success, 'You have been successfully registered!. Please wait for the admin approval"); window.location.href='../index.php';
        </script>
<?php
}
?>




</div>

 

</body>
</html>
