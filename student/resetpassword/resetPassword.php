<?php
include"../connection.php";

if (!isset($_GET["code"])) {
	exit("can't find the page");
}
$code=$_GET["code"];
$getEmailQuery=mysqli_query($link,"SELECT email FROM resetpasswords WHERE code='$code'");
if (mysqli_num_rows($getEmailQuery)==0) {
	exit("can't find page");
}
if (isset($_POST["password"])) {
	$pw=$_POST["password"];
	$row=mysqli_fetch_array($getEmailQuery);
	$email=$row["email"];
	$query=mysqli_query($link,"UPDATE student_registration SET password='$pw' WHERE email='$email'");
	if ($query) {
		$query=mysqli_query($link,"DELETE FROM resetpasswords WHERE code='$code'");
		exit("<h1 style='color:#0000FF; text-align:center;'><strong>Password Updated</strong></h1>");
	}else{
		exit("<h1 style='color:#FF0000; text-align:center;'><strong>Something Went Wrong</strong></h1>");
	}
}
?>
<head>
    <title>Users Login Form</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <img class="wave" src="../img/wave.png">
    <div class="container">
        <div class="img">
            <img src="../img/bg.svg">
        </div>
        <div class="login-content">
            <form name="form1" action="" method="POST">
                <img src="../img/avatar.svg">
                <h2 class="title">Update Password</h2>
                <div class="input-div one">
                   <div class="i">
                        <i class="fas fa-user"></i>
                   </div>
                   <div class="div">
                        
                        <input type="password" class="input" placeholder="New Password" name="password" autocomplete="off" >
                   </div>
                </div>
                <input type="submit" class="btn" value="Update Password" name="submit">
            </form>
        </div>
    </div>
    <script type="text/javascript" src="../js/main.js"></script>
<!-- <form name="form1" method="POST">
	<input type="password" name="password" placeholder="New Password">
	<input type="submit" name="submit" value="Update Password">
</form> -->