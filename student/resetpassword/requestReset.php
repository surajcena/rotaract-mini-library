<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
include"../connection.php";

if (isset($_POST["email"])) {

    $emailTo=$_POST["email"];
    $code=uniqid(true);
    $query=mysqli_query($link,"INSERT INTO resetpasswords(code,email) VALUES ('$code','$emailTo')");
    if (!$query) {
        exit("Error");
    }
        // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'rotaractminilibrary@gmail.com';                     // SMTP username
        $mail->Password   = 'rotaract@2020';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom('rotaractminilibrary@gmail.com', 'Rotaract Mini Library');
        $mail->addAddress($emailTo);     // Add a recipient              
        $mail->addReplyTo('no-reply@gmail.com', 'No Reply');

        

        // Content
        $mail->isHTML(true);
        $url="http://".$_SERVER["HTTP_HOST"].dirname($_SERVER["PHP_SELF"])."/resetPassword.php?code=$code";                                  
        $mail->Subject = 'Your password reset link';
        $mail->Body    = "<h1>You requested a password reset</h1>
                          Click <a href='$url'>this link</a> to do so ";
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo ' <h1 style="color:#FF0000; text-align:center;"><strong>Reset Password link has been sent to your email.Please check your email.</strong></h1>';
    } catch (Exception $e) {
        echo " <h1 style='color:#FF0000; text-align:center;'><strong>Message could not be sent. Mailer Error: {$mail->ErrorInfo}</strong></h1>";
    }
    exit();
}



?>
<head>
    <title>Users Login Form</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>

<body>
    <img class="wave" src="../img/wave.png">
    <div class="container">
        <div class="img">
            <img src="../img/bg.svg">
        </div>
        <div class="login-content">
            <form name="form1" action="" method="post">
                <img src="../img/avatar.svg">
                <h2 class="title">Forgot Password</h2>
                <h4>Notice:If your previous email don't match this email than your password will not update.</h4>
                <div class="input-div one">
                   <div class="i">
                        <i class="fas fa-user"></i>
                   </div>
                   <div class="div">
                        
                        <input type="text" class="input" placeholder="Email" name="email" autocomplete="off" >
                   </div>
                </div>
                <input type="submit" class="btn" value="Reset Password" name="submit">
            </form>
        </div>
    </div>

<!-- <form method="POST">
   <input type="text" name="email" placeholder="Email" autocomplete="off">
    <br>
    <input type="submit" name="submit" value="Reset Email">
</form> -->
  