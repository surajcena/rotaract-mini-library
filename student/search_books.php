<?php
   session_start();
 if (!isset($_SESSION["username"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 include"header.php";
 include"connection.php";
 ?>
 <!-- page content area main -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Library Management System</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="row" style="min-height:500px">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Search Books</h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <form name="form1" class="form1" method="post">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                <input type="text" name="t1" placeholder="Enter books name" required="" class="form-control">
                                            </td>
                                            <td>
                                                <input type="submit" name="submit1" value="search books" class="form-control btn btn-default">
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </form>
                                <?php
                                if (isset ($_POST["submit1"])) {
                                 $i=0;

                                 $res=mysqli_query($link,"SELECT * FROM add_books WHERE books_name like('%$_POST[t1]%') ");
                                 echo "<table class='table table-bordered'>";
                                 echo "<tr>";
                                 while ($row=mysqli_fetch_array($res)) {
                                    $i=$i+1;
                                    echo "<td>";

                                    ?> <img src="../librarian/<?php echo $row["books_image"] ?>" height="100" width="100"> <?php
                                    
                                    echo "<br>";
                                    echo "<b>" .$row["books_name"]."</b>";
                                     echo "<br>";
                                    echo "<b>"."Available:" .$row["books_available_qty"]."</b>";
                                    echo "<br>";
                                    $id=$row['id'];
                                    echo "<button data-id='".$id."' class='userinfo'>More Details</button>";
                                     echo "</td>";
                                     if($i==2){
                                        echo "</tr>";
                                        echo "<tr>";
                                        $i=0;
                                     }
                                 
                                 echo "</tr>";
                                 echo "</table>";
                                  }  
                                }
                                else{
                                     $i=0;

                                 $res=mysqli_query($link,"SELECT * FROM add_books WHERE books_available_qty>0");
                                 echo "<table class='table table-bordered'>";
                                 echo "<tr>";
                                 while ($row=mysqli_fetch_array($res)) {
                                    $i=$i+1;
                                    echo "<td>";

                                    ?> <img src="../librarian/<?php echo $row["books_image"] ?>" height="100" width="100"> <?php
                                    
                                    echo "<br>";
                                    echo "<b>" .$row["books_name"]."</b>";
                                     echo "<br>";
                                    echo "<b>"."Available:" .$row["books_available_qty"]."</b>";
                                    echo "<br>";
                                    $id=$row['id'];
                                    echo "<button data-id='".$id."' class='userinfo'>More Details</button>";
                                     echo "</td>";
                                     if($i==2){
                                        echo "</tr>";
                                        echo "<tr>";
                                        $i=0;
                                     }
                                 }
                                 
                                 echo "</tr>";
                                 echo "</table>";

                                }
                               
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <div class="container" >
   <!-- Modal -->
   <div class="modal fade" id="empModal" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Books Additional Information</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
 
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
     </div>
    </div>
   </div>

<script type="text/javascript">
   $(document).ready(function(){

 $('.userinfo').click(function(){
   
   var userid = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'ajaxfile.php',
    type: 'post',
    data: {userid: userid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#empModal').modal('show'); 
    }
  });
 });
});
    
</script>

        <!-- /page content -->
<?php
include"footer.php";
?>