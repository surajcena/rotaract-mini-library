<?php
session_start();
include'connection.php';
?>
<html lang="en">
<head>
    <title>Users Login Form</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <img class="wave" src="img/wave.png">
    <div class="container">
        <div class="img">
            <img src="img/bg.svg">
        </div>
        <div class="login-content">
            <form name="form1" action="" method="post">
                <img src="img/avatar.svg">
                <h2 class="title">Welcome</h2>
                <div class="input-div one">
                   <div class="i">
                        <i class="fas fa-user"></i>
                   </div>
                   <div class="div">
                        
                        <input type="text" class="input" placeholder="Username" name="username" required="">
                   </div>
                </div>
                <div class="input-div pass">
                   <div class="i"> 
                        <i class="fas fa-lock"></i>
                   </div>
                   <div class="div">
                        
                        <input type="password" class="input" placeholder="password" name="password" required="">
                   </div>
                </div>
                <a href="./resetpassword/requestReset.php">Forgot Password??</a>
                <input type="submit" class="btn" value="Login" name="submit1"><br>
                
                <!-- <input type="button" class="btn" onclick="location.href='https://google.com';" value="Go to Google" /> -->
                <a style="text-align: center;" href="./registration.php">Didn't have an account??</a>
            </form>
        </div>
    </div>
<?php
if(isset($_POST["submit1"])){
    $count=0;
    $res=mysqli_query($link,"SELECT * FROM student_registration WHERE username='$_POST[username]' && password='$_POST[password]' && status='yes'"); 
    $count=mysqli_num_rows($res);
    if ($count==0) {
        ?>
        <div class="alert alert-danger col-lg-6 col-lg-push-3">
    <strong style="color:red;">Invalid</strong> Username Or Password Or your Account Is Not Approved.
</div>
<?php
    }
    else
    {
        
        $_SESSION["username"]=$_POST["username"];
?>

<script type="text/javascript">
    window.location="search_books.php";
</script>
 <script type="text/javascript" src="js/main.js"></script>
<?php
}
}
?>

</body>
</html>
