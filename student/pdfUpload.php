<?php
  session_start();
 if (!isset($_SESSION["username"])) {
     
     ?>
     <script type="text/javascript">
         window.location="login.php";
     </script>

     <?php
 }
 include"header.php";
 include"connection.php";
 $res=mysqli_query($link,"UPDATE messages SET read1='y' WHERE dusername='$_SESSION[username]'");
 ?>
 <?php

// fetch files
$sql = "select filename from tbl_files";
$result = mysqli_query($link, $sql);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Rotaract Mini Library</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" >
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" /> -->
</head>
<body style="background-color: #fff;">
<br/>
<div class="container">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-3 ">
            <h4>If you have any interesting content please share.</h4>
        <form action="pdfUploadLogic.php" method="post" enctype="multipart/form-data">
            <legend>Select File to Upload:(pdf, txt, doc, docx, png, jpg, jpeg,  gif)</legend>
            <div class="form-group">
                <input type="file" name="file1" />
            </div>
            <input type="text" name="docname" placeholder="File Name">
            <input type="text" name="auther" placeholder="Your Name">
            <div class="form-group">
                <input type="submit" name="submit" value="Upload" class="btn btn-info"/>
            </div>
            <?php if(isset($_GET['st'])) { ?>
                <div class="alert alert-success text-center">
                <?php if ($_GET['st'] == 'success') {
                        echo "File Uploaded Successfully!";
                    }
                    else
                    {
                        echo 'Invalid File Extension!';
                    } ?>
                </div>
            <?php } ?>
        </form>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-8 col-xs-offset-3">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>FileName</th>
                        <!-- <th>View</th> -->
                        <th>Download</th>
                        <th>UploadedBy</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                $result=mysqli_query($link,"SELECT * FROM tbl_files");
                while($row = mysqli_fetch_array($result)) { ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <!-- <td><a href="uploads/<?php echo $row['filename']; ?>" target="_blank">View</a></td> -->
                    <td><a href="uploads/<?php echo $row['filename']; ?>" download>Download</td>
                    <td><?php echo $row['auther']; ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
<?php
include"footer.php";
?>